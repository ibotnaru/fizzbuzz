################################################################
#
#
#	login: 	        ibotnaru
#	name:	        Recursion
#	description:    Solve "FizzBuzz" using recursion.
#
#	date:	08.05.2019
################################################################

n = 1

def fizzbuzz_rec(n):

        if n <= 100:
            if n % 3 == 0:
                if n % 5 == 0:
                    print("FizzBuzz")
                print("Fizz")
            elif n % 5 == 0:
                print("Buzz")
            else:
                print("{0}".format(n))
            n = n + 1
            fizzbuzz_rec(n)

fizzbuzz_rec(n)